#include "LowPower.h"

#define debug false // aktiviert/deaktiviet die serielle Ausgabe und die LED

//Sender
#include <VirtualWire.h>
#define MESSAGE_LEN 10
char msg [MESSAGE_LEN] ;
int msg_len = MESSAGE_LEN;
int tx_pin = 10;


//DHT 22
#include <SimpleDHT.h>
#define DHTPIN 2                       //Sensor an Pin 2
SimpleDHT22 DHT;
#define DHTPOWERPIN 3


void setup()
{
  if (debug)
  {
    Serial.begin(115200);
    Serial.println("Wetterstation - Sender");
    pinMode(LED_BUILTIN, OUTPUT);
  }

  pinMode(DHTPOWERPIN, OUTPUT);

  //Sender
  pinMode(tx_pin, OUTPUT);
  vw_set_tx_pin(tx_pin);                    // Übertragungspin, der die Geschwindigkeit regelt
  vw_setup(600);                     // Bits pro Sekunde

  pinMode(DHTPIN, INPUT_PULLUP);

}


void loop()
{
  // Read DHT
  digitalWrite(DHTPOWERPIN , HIGH);
  if (debug)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);
    delay(1000);
    digitalWrite(LED_BUILTIN, HIGH);
  }
  else
  {
    delay(2000);
  }

  float Luftfeuchtigkeit = 0;       //Luftfeuchtigkeit auslesen und unter Temperatur speichern
  float Temperatur = 0;            //Temperatur auslesen und unter Temperatur speichern

  while (  !DHT.read2(DHTPIN, &Temperatur, &Luftfeuchtigkeit, NULL)    )  //liefert bei Erfolg eine 0 und bei fehler eine 1
  {
    delay (100); // bei Fehler etwas warten und nochmal messen bis die messung erfolgreich war

  }

  digitalWrite(DHTPOWERPIN , LOW);
  if (debug)
  {
    // Serial Output
    Serial.println("--------------------");
    Serial.print("Luftfeuchtigkeit\t: ");
    Serial.print(Luftfeuchtigkeit);
    Serial.println(" %");
    Serial.print("Temperatur\t \t: ");
    Serial.print(Temperatur);
    Serial.println(" Grad Celsius");
  }
  // Nachricht für Funkübertragung vorbereiten
  sprintf(msg, "%d ; %d", int(10 * Temperatur), int ( 10 * Luftfeuchtigkeit  ));
  if (debug)
  {
    Serial.print("Message: ");
    Serial.print(msg);
    Serial.print("  Length: ");
    Serial.println(msg_len);
  }

  // Nachricht über Funkstrecke senden
  vw_send((uint8_t *)msg,  msg_len );
  vw_wait_tx();

  // Energie Sparen
  digitalWrite(LED_BUILTIN, LOW); // LED aus
  LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF); // Eine weile in den Teifschlaf gehen
}


