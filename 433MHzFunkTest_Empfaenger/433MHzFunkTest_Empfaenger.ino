#include <VirtualWire.h>

//Empfänger
int i;
#define MESSAGE_LEN 10

uint8_t buf[MESSAGE_LEN];
uint8_t buflen = MESSAGE_LEN;
int rx_pin = 2;

void setup()
{
  Serial.begin(115200);
  pinMode(2, INPUT);
  vw_setup(600);                   // Bits pro Sekunde
  vw_set_rx_pin(2);                // Empfangsstift
  vw_rx_start();                  // Empfängerprozess aktivieren. Diese muss man aufrufen bevor ein Empfang möglich ist
  Serial.println("433MHz Funk Test - Empfaenger");
}

void loop()
{
  if (vw_get_message(buf, &buflen))
  {
    for (i = 0; i < buflen; i++)
    {
      Serial.print((char)buf[i]);
    }
    Serial.println(" - ENDE -  ");
  }
}
