// Empfaenger
#include <VirtualWire.h>
#define MESSAGE_LEN 10
uint8_t buf[MESSAGE_LEN];
uint8_t buflen = MESSAGE_LEN;
int rx_pin = 2;
int i = 0;
int messageNr = 0;

String inString = "";

//Display
#include <U8g2lib.h> //Achtung unbedingt aktuelle Blibiothek von bitbucket laden!!!

//U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8( /* clock=*/ SCL, /* data=*/  SDA, /* reset=*/ U8X8_PIN_NONE);  //kleines Display
U8X8_SH1106_128X64_NONAME_HW_I2C u8x8(/* reset=*/ U8X8_PIN_NONE);; //grosses Display
char tmpChars[5];

float a  ;
float b ;

void setup()
{
  Serial.begin(115200);
  Serial.println("Wetterstation - Empfaenger");
  Serial.print("Init ... ");

  //Display
  u8x8.begin();
  u8x8.setPowerSave(0);
  u8x8.clear();

  // Empfaenger
  pinMode(rx_pin, INPUT);
  vw_setup(600);                   // Bits pro Sekunde
  vw_set_rx_pin(rx_pin);           // Empfangsstift
  vw_rx_start();                   // Empfängerprozess aktivieren. Diese muss man aufrufen bevor ein Empfang möglich ist

  Serial.println("Done");
}




#define R  8314.3   // J/(kmol*K) (universelle Gaskonstante)
#define mw  18.016  // kg/kmol    (Molekulargewicht des Wasserdampfes)

float TaupunktF (float r, float T)
{
  // # Parameter für für über , oder unter 0°C wählen
  if (T >= 0)
  {
    a = 7.5;
    b = 237.3;
  }
  else
  {
    a = 7.6;
    b = 240.7;
  }

  float SDD = 6.1078 *   pow(   10,  ((a * T) / (b + T)) )    ; // Saettigungsdampfdruck in hPa
  float DD = r / 100 * SDD ;                     // Dampfdruck in hPa
  float v = log10(DD / 6.1078);
  float TD = b * v / (a - v);                   // Taupunkttemperatur in °C

  return TD;
}


void loop()
{


  if (vw_get_message(buf, &buflen))
  {
    inString = buf;

    float Temperatur = float( inString.toInt()) / 10;
    inString =  inString.substring(   inString.indexOf(";") + 1 , inString.length() );
    float Luftfeuchtigkeit  =    float(inString.toInt()) / 10;
    float Taupunkt = TaupunktF(Luftfeuchtigkeit, Temperatur);

    messageNr++;
    // Serial Output
    Serial.println("--------------------");
    Serial.print("recieve message no. ");   Serial.println(messageNr);
    Serial.print("Luftfeuchtigkeit\t: ");   Serial.print(Luftfeuchtigkeit); Serial.println(" %");
    Serial.print("Temperatur\t \t: ");      Serial.print(Temperatur);       Serial.println(" Grad Celsius");
    Serial.print("Taupunkt\t \t: ");        Serial.print(Taupunkt);         Serial.println(" Grad Celsius");

    //  Display

    u8x8.setFont(u8x8_font_chroma48medium8_r);
    //u8x8.setFlipMode(1);
    u8x8.setInverseFont(1);

    u8x8.drawString(0, 0, "  Aussen-werte  ");
    u8x8.setInverseFont(0);


    dtostrf(Temperatur, 3, 1, tmpChars);

    u8x8.draw2x2String(1,  2, tmpChars );
    u8x8.drawString  (10, 3, "Grad C");


    dtostrf(Luftfeuchtigkeit, 3, 1, tmpChars);

    u8x8.draw2x2String(1,  4, tmpChars );

    u8x8.drawString (10,  5, "% rh");
    

    dtostrf(Taupunkt, 3, 1, tmpChars);

    u8x8.drawString(0,  7, "Taup. " );
    u8x8.drawString(5,  7, tmpChars );
    u8x8.drawString(10,  7, "Grad C" );

    //    dtostrf(messageNr, 5, 0, tmpChars);
    //    u8x8.drawString(0,  1, tmpChars );

  }


}

