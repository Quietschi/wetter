//Sender
#include <VirtualWire.h>
char msg [VW_MAX_MESSAGE_LEN] ;
int msg_len = 0;
int tx_pin = 10;


void setup() {
  Serial.begin(115200);
  Serial.println("433MHz Funk Test - Sender");
  pinMode(LED_BUILTIN, OUTPUT);
  
  //Sender
  pinMode(tx_pin, OUTPUT);
  vw_set_tx_pin(tx_pin);                  // Übertragungspin, der die Geschwindigkeit regelt
  vw_setup(600);                   // Bits pro Sekunde

}

void loop() {
  msg_len = Serial.available();
  if ( msg_len > 0) {

    //  *msg = Serial.readString();
    Serial.readBytes( msg,  msg_len  );

    digitalWrite(LED_BUILTIN, HIGH);
    //der Text der in der Variablen "msg" gespeichert ist, wird mittels Angabe der Länge, versendet
    vw_send((uint8_t *)msg,  msg_len);
    //nach dem Senden wird gewartet bis der Sender wieder bereit ist
    vw_wait_tx();


Serial.println("-----------");
    Serial.print("Send ");
    Serial.print(msg_len);
    Serial.println(" Bytes. Message was :");
    Serial.write(msg,  msg_len);
    

     msg_len = 0 ;
    digitalWrite(LED_BUILTIN, LOW);
  }
  delay(2000);

}
