#include <VirtualWire.h>

//Empfänger
int i;
uint8_t buf[VW_MAX_MESSAGE_LEN];
uint8_t buflen = VW_MAX_MESSAGE_LEN;
int rx_pin = 2;

//Sender
char *msg = "test";
int tx_pin = 10;

void setup() {

  Serial.begin(115200);
  
  pinMode(2, INPUT);
  vw_setup(4800);                   // Bits pro Sekunde
  vw_set_rx_pin(2);                // Empfangsstift
  vw_rx_start();                  // Empfängerprozess aktivieren. Diese muss man aufrufen bevor ein Empfang möglich ist
//Sender
  vw_set_tx_pin(10);                  // Übertragungspin, der die Geschwindigkeit regelt
  vw_setup(4800);                   // Bits pro Sekunde
Serial.println("433MHz Funk Test");
}

void loop() {
//der Text der in der Variablen "msg" gespeichert ist, wird mittels Angabe der Länge, versendet
  vw_send((uint8_t *)msg, strlen(msg));
//nach dem Senden wird gewartet bis der Sender wieder bereit ist
  vw_wait_tx();
  
  byte message[VW_MAX_MESSAGE_LEN]; 
  byte messageLength = VW_MAX_MESSAGE_LEN; //die Größe der Nachricht
//falls Empfangsdaten vorhanden sind, werden sie in "buf" kopiert
   if (vw_get_message(buf, &buflen))
//Anschließend werden die empfangenen Daten Zeichen für Zeichen bis zur max definierten Länge einzeln auf der 
//seriellen Schnittstelle ausgegeben 
   { 
    for (i = 0; i < buflen; i++) 
    {
    Serial.print((char)buf[i]);                       // Nachricht aus buf ausgeben
    }
    Serial.println("");
  }


    

}
